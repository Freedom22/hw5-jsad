

        class Card {
            constructor(post) {
                this.post = post;
            }

            createCardElement() {
                const cardDiv = document.createElement('div');
                cardDiv.classList.add('card');
                cardDiv.innerHTML = `
                    <h2>${this.post.title}</h2>
                    <p>${this.post.body}</p>
                    <p><strong>Author:</strong> ${this.post.user.name} ${this.post.user.surname}</p>
                    <p><strong>Email:</strong> ${this.post.user.email}</p>
                    <button onclick="deletePost(${this.post.id})">Delete</button>
                `;
                return cardDiv;
            }
        }

        
        async function fetchData() {
            try {
                const [usersResponse, postsResponse] = await Promise.all([
                    fetch('https://ajax.test-danit.com/api/json/users'),
                    fetch('https://ajax.test-danit.com/api/json/posts')
                ]);

                const [users, posts] = await Promise.all([
                    usersResponse.json(),
                    postsResponse.json()
                ]);

                displayPosts(posts, users);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }

        function displayPosts(posts, users) {
            const postsContainer = document.getElementById('posts-container');
            postsContainer.innerHTML = '';

            posts.forEach(post => {
                const user = users.find(user => user.id === post.userId);
                if (user) {
                    const card = new Card({ ...post, user });
                    const cardElement = card.createCardElement();
                    postsContainer.appendChild(cardElement);
                }
            });
        }

       
        async function deletePost(postId) {
            try {
                const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
                    method: 'DELETE'
                });
                if (response.ok) {
                    const cardToRemove = document.querySelector(`.card[data-post-id="${postId}"]`);
                    if (cardToRemove) {
                        cardToRemove.remove();
                    } else {
                        console.error('Card not found');
                    }
                } else {
                    console.error('Delete request failed');
                }
            } catch (error) {
                console.error('Error deleting post:', error);
            }
        }

        window.onload = fetchData;
   
